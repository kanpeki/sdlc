package ro.kanpeki.jms.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	public void sentToMongo(String json) {
		logger.info("Sending to MongoDB" );
		MongoClient client = new MongoClient();
		DB db = client.getDB("vendor");
		DBCollection collection = db.getCollection("client");
		//MongoDatabase db = client.getDatabase("vendor");
		//MongoCollection collection = db.getCollection("client");
		logger.info("Converting JSON to DBObject");
		DBObject object = (DBObject) JSON.parse(json);
		collection.insert(object);
		logger.info("Sent to MongoDB");
		
		
		
	}

}
