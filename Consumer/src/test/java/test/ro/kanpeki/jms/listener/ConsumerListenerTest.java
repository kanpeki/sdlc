package test.ro.kanpeki.jms.listener;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
//import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ro.kanpeki.jms.listener.ConsumerListener;

public class ConsumerListenerTest {
	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener) context.getBean("consumerListener");	
		message = createMock(TextMessage.class);
		
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext)context).close();
	}

	@Test
	public void testOnMessage() {
		//ConsumerListener listener = new ConsumerListener();
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		//assertNull(message);
		verify(message);
	}

}
