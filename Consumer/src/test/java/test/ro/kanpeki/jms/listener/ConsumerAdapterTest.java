package test.ro.kanpeki.jms.listener;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ro.kanpeki.jms.listener.ConsumerAdapter;

public class ConsumerAdapterTest {
	public String json = "{vendorName:\"MicrosoftTest3\", firstName: \"IuliaTest3\", lastName: \"SarguTest3\"}";
	
	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		ConsumerAdapter adapter = new ConsumerAdapter();
		adapter.sentToMongo(json);
		assertNotNull(json);
		
	}

}
